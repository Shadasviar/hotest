import TestCases
import Network.Simple.TCP
import Hotest.API
import Hotest.Datagram
import System.Exit
import qualified Data.ByteString as BS
import qualified Data.ByteString.Char8 as BSC 

main = do
    mapM_ testSession sesTestCases
    
    connect "127.0.0.1" "6666" $ \(sock, addr) -> do
        putStrLn "Run admin tests ... "
        let (_, creds, expectedAnswer) = head sesTestCases
        let newUser = datagram ADD_USER $ opSesData ["{\"login\":\"t.user\",\"password\":\"test\",\"name\":\"Test\",\"surname\":\"Test\"}"]
        let retNewUser = datagram ERROR_DATAGRAM $ BS.pack [fromErrCode SUCCESS, fromCmd ADD_USER]
           
        writeDatagram sock creds
        res <- readDatagram sock
        if res == expectedAnswer
           then putStrLn "Connected ... "
           else die "Connection to server failed"
           
                
        writeDatagram sock newUser
        res <- readDatagram sock
        if res == retNewUser
           then putStrLn "Created test user"
           else die "Cant create test user"
           
        mapM_ (runTest sock) testCasesAdmin
        closeSock sock
    
    connect "127.0.0.1" "6666" $ \(sock, addr) -> do
        putStrLn "Run tests set as t.user test ... "
        let (_, creds, expectedAnswer) = last sesTestCases
        
        writeDatagram sock creds
        res <- readDatagram sock
        if res == expectedAnswer
           then putStrLn "Connected ... "
           else die "Connection to server failed"
        mapM_ (runTest sock) testCasesTestUser
        closeSock sock
        
    connect "127.0.0.1" "6666" $ \(sock, addr) -> do
        putStrLn "Clean after tests ..."
        
        let (_, creds, expectedAnswer) = head sesTestCases
        let deleteTestUser = datagram DELETE_USER $ BSC.pack "t.user"
        let ret = datagram ERROR_DATAGRAM $ BS.pack [fromErrCode SUCCESS, fromCmd DELETE_USER]
           
        writeDatagram sock creds
        res <- readDatagram sock
        if res == expectedAnswer
           then putStrLn "Connected ... "
           else die "Connection to server failed"
           
        writeDatagram sock deleteTestUser
        res <- readDatagram sock
        if res == ret
           then putStrLn "Done"
           else die "Cant clean after tests."
           
        closeSock sock
    
    putStrLn "Post-control OPEN_SESSION test ..."    
    mapM_ testSession sesTestCases
