var searchData=
[
  ['database',['Database',['../class_database.html',1,'']]],
  ['datagram',['Datagram',['../struct_hotest_protocol_1_1_datagram.html',1,'HotestProtocol::Datagram'],['../struct_hotest_protocol_1_1_datagram.html#ad3ceedc5da06e9385e8b29d12521e951',1,'HotestProtocol::Datagram::Datagram()']]],
  ['deletegroup',['deleteGroup',['../class_database.html#a4c2ea4a00c6a212440abee5ca9c9b6dc',1,'Database']]],
  ['deleteuser',['deleteUser',['../class_database.html#a0db3ff3c099b706cc1d8369762aec1cc',1,'Database']]],
  ['deprecated_20list',['Deprecated List',['../deprecated.html',1,'']]],
  ['diff',['diff',['../classnlohmann_1_1basic__json.html#a543bd5f7490de54c875b2c0912dc9a49',1,'nlohmann::basic_json']]],
  ['difference_5ftype',['difference_type',['../classnlohmann_1_1detail_1_1iter__impl.html#a2f7ea9f7022850809c60fc3263775840',1,'nlohmann::detail::iter_impl::difference_type()'],['../classnlohmann_1_1basic__json.html#afe7c1303357e19cea9527af4e9a31d8f',1,'nlohmann::basic_json::difference_type()']]],
  ['discarded',['discarded',['../namespacenlohmann_1_1detail.html#a1ed8fc6239da25abcaf681d30ace4985a94708897ec9db8647dfe695714c98e46',1,'nlohmann::detail']]],
  ['diyfp',['diyfp',['../structnlohmann_1_1detail_1_1dtoa__impl_1_1diyfp.html',1,'nlohmann::detail::dtoa_impl']]],
  ['dump',['dump',['../classnlohmann_1_1detail_1_1serializer.html#a95460ebd1a535a543e5a0ec52e00f48b',1,'nlohmann::detail::serializer::dump()'],['../classnlohmann_1_1basic__json.html#a5adea76fedba9898d404fef8598aa663',1,'nlohmann::basic_json::dump()']]]
];
